rm -rf /opt/ANDRAX/upnp_tools

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/python3 setup.py install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Setup install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
